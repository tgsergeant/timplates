import sys
import template

__author__ = 'tim'

class Person:
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender
        self.friends = []

    def __repr__(self):
        return "{}, {} y.o. {!s} {!s}".format(self.name, self.age, self.gender, self.friends)


def main():
    john = Person('John Egbert', 13, 'M')
    rose = Person('Rose', 12, 'F')
    dave = Person('Dave', 13, 'M')
    jade = Person('Jade', 13, 'F')

    john.friends.append(rose)
    john.friends.append(dave)
    john.friends.append(jade)

    context = {'person':john}

    template.render_template('example.html', sys.stdout, context)


if __name__ == '__main__':
    main()