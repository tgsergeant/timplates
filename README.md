A simple templating language in a fairly similar style to Django. Probably the most notable difference is that there is only one 'end' tag, which takes the tag to be closed as an argument (ie, {% end for %} instead of {% endfor %}.

Built using a tutorial in preparation for NCSS 2013.

Features:
---

* Include tag
* If, for statements
* Let tag

TODO
---

* Party?