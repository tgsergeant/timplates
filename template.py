import html

class TemplateException(Exception):
    pass

class Node:
    def __init__(self, parent):
        self.parent = parent

    def _eval(self, expr, context):
        return eval(expr, {}, context)

    def render(self, ostream, context):
        raise NotImplementedError


class TextNode(Node):
    def __init__(self, parent, content):
        super(TextNode, self).__init__(parent)
        self.content = content

    def render(self, ostream, context):
        ostream.write(self.content)

class GroupNode(Node):
    def __init__(self, parent):
        super(GroupNode, self).__init__(parent)
        self.children = []

    def render(self, ostream, context):
        for child in self.children:
            child.render(ostream, context)

class PythonNode(Node):
    def __init__(self, parent, expr):
        super(PythonNode, self).__init__(parent)
        self.expr = expr

    def render(self, ostream, context):
        ret = self._eval(self.expr, context)
        if ret:
            ostream.write(html.escape(str(ret)))


class IncludeNode(Node):
    def __init__(self, parent, arg):
        super().__init__(parent)

        self.child = compile_template(arg)
        pass

    def render(self, ostream, context):
        self.child.render(ostream, context)

class IfNode(Node):
    def __init__(self, parent, predicate):
        super().__init__(parent)
        self.predicate = predicate
        self._if = GroupNode(self)
        self._else = GroupNode(self)

    def render(self, ostream, context):
        ret = self._eval(self.predicate, context)
        if ret:
            self._if.render(ostream, context)
        else:
            self._else.render(ostream, context)

class ForLoopCounter():
    def __init__(self, i, length):
        self.i = i
        self.length = length
        self.first = i == 0
        self.last = i == length - 1

class ForNode(Node):
    def __init__(self, parent, src, dest):
        super().__init__(parent)
        self.src = src
        self.dest = dest
        self._for = GroupNode(self)


    def render(self, ostream, context):
        src = self._eval(self.src, context)
        for counter, item in enumerate(src):
            forloop = ForLoopCounter(counter, len(src))
            context['forloop'] = forloop
            context[self.dest] = item
            self._for.render(ostream, context)

class LetNode(Node):
    def __init__(self, parent, name, expression):
        super().__init__(parent)
        self.name = name
        self.expression = expression


    def render(self, ostream, context):
        context[self.name] = self._eval(self.expression, context)


def parse_include(parent, arg):
    if not arg:
        raise TemplateException("'include' tag missing filename argument")
    return IncludeNode(parent, arg)

def parse_template(raw, upto, parent, parent_tag):
    prev, L = upto, len(raw)

    while upto < L:
        #Render tag
        if raw[upto:upto + 2] == '{{':
            #Catch the text up to here
            node = TextNode(parent, raw[prev:upto])
            parent.children.append(node)

            #Read until the end of the tag
            upto += 2
            prev = upto
            while raw[upto:upto+2] != "}}":
                upto += 1

            node = PythonNode(parent, raw[prev:upto].strip())
            parent.children.append(node)
            upto += 2
            prev = upto

        #Proper tag
        elif raw[upto:upto + 2] == '{%':
            node = TextNode(parent, raw[prev:upto])
            parent.children.append(node)

            #Extract tag and argument
            upto += 2
            while raw[upto].isspace():
                upto += 1

            prev = upto

            while not raw[upto].isspace():
                upto += 1

            tag = raw[prev:upto].strip()

            while raw[upto].isspace():
                upto += 1

            prev = upto

            while raw[upto:upto+2] != "%}":
                upto += 1

            arg = raw[prev:upto].strip()
            upto += 2

            if tag == 'include':
                if not arg:
                    raise TemplateException("'include' tag missing filename argument")
                node =  IncludeNode(parent, arg)
                parent.children.append(node)
            elif tag == 'end':
                if arg != parent_tag:
                    raise TemplateException('Unexpected end of tag {!r} while parsing tag {!r}.'.format(tag, parent_tag))
                #End the recursion
                return upto

            elif tag == 'if':
                node = IfNode(parent, arg)
                parent.children.append(node)
                upto = parse_template(raw, upto, node._if, tag)

            elif tag == 'for':
                dest, src = arg.split(' in ', 1)
                node = ForNode(parent, src.strip(), dest.strip())
                parent.children.append(node)
                upto = parse_template(raw, upto, node._for, tag)

            elif tag == 'else':
                if parent_tag == 'if':
                    return parse_template(raw, upto, parent.parent._else, parent_tag)
                else:
                    raise TemplateException('"else" tag encountered in an invalid tag: {!r}'.format(parent_tag))
            elif tag == 'let':
                name, expr = arg.split('=', 1)
                node = LetNode(parent, name.strip(), expr.strip())
                parent.children.append(node)
            else:
                raise TemplateException("Invalid tag name {!r}".format(tag))
            prev = upto

        else:
            upto += 1

    node = TextNode(parent, raw[prev:upto])
    parent.children.append(node)


def compile_template(path):
    root = GroupNode(None)
    with open(path) as f:
        parse_template(f.read(), 0, root, None)
    return root

def render_template(path, ostream, context):
    node = compile_template(path)
    node.render(ostream, context)